Git can't typically tell the difference between the renaming (or more generally, moving) a file, versus deletion of the old file name and the creation of the new one. Tracking renames is important, because it helps maintain the file's history. There are workarounds (like `git log --follow`), but they don't work with most IDEs that I know of.

Git *does* try to infer moves, by looking at the diff similarity index between deleted and added files. However, this doesn't catch everything. One common example I run into is when moving a ruby file into a subfolder `foo`. This is a rename, but the Ruby module convention also requires me to wrap the file's contents in a `module Foo`. This introduces an extra layer of indentation to almost all the lines, and blows up the diff, thereby breaking git's move inference.

I'm afraid that `git mv` isn't some magical solution to this, either. It appears to just be a near-useless shortcut for `mv oldname newname ; git add newname ; git rm oldname`. I say near-useless, because everybody just does `git add .` anyway lol.

In this repository I make the same change, moving `original_file.txt` to `moved_file.txt` and changing the contents (from ABC to 123). Neither `mv` nor `git mv` tracks this change. You can see for yourself by comparing the `normal_mv` and `git_mv` branches.

The steps I followed are listed in `creation script.sh`.
