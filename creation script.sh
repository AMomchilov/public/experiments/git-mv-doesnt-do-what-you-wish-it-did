#!/bin/bash

cd ~/Desktop/
mkdir "test_repo"
cd "test_repo"

git init
git checkout -b "original_file_state"

# Set up the initial file state
echo -e "a\nb\nc" > "original_file.txt"
git add original_file.txt
git commit -m "created original file"

# Renaming via normal mv
git checkout -b "normal_mv" "original_file_state"
mv "original_file.txt" "moved_file.txt" # move normally
echo -e "1\n2\n3" > "moved_file.txt" # drastically change the contents
git add "original_file.txt" # Add the file's deletion to staging area
git add "moved_file.txt" # Add the file's modification to staging area
git commit -m "renamed via normal mv"

# Renaming via git mv
git checkout -b "git_mv" "original_file_state"
git mv "original_file.txt" "moved_file.txt" # git mv
echo -e "1\n2\n3" > "moved_file.txt" # drastically change the contents
git add "moved_file.txt" # Add the file's modification to staging area
git commit -m "renamed via git mv"